import masto from 'megalodon';
import { accessToken, baseURL } from './data';

const client = new masto.default(accessToken, baseURL);

export const sendToot = message => {
    client.post('/statuses', {status: message})
	.then(res => {console.log(res.data)});
};
